// JavaScript Document
var box = 999;
alert (box);
alert (typeof(box));//number

//可以重复赋予不同类型的值
box = 'A';//由于上面已经定义了box  所以此时var可不用加
alert (box);
alert (typeof(box));//string
//这种做法通常不建议使用  性能不高
box = "这又是一种类型";
alert (box);
alert (typeof box);//括号可以不带

//分号可以省略   但不是很专业
var number = 456;
var number = 789//重复定义相同的变量名 不是很专业
alert(number);//789


var num1 = 123,
	num2 = 456,
	num3 = '',
	num;
alert (num1);