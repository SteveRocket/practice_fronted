// JavaScript Document

















var box = {};//创建一个空的对象

if(box != null){//不等于null 说明不是空对象
		alert("对象创建成功！");
	}
	
var box = '';
var box = 0;
var box = false;//使用false或true

alert (undefined == null);//true
alert (typeof undefined == typeof null);//false




/*
var box;//Undefined类型
alert(box);//值是undefined
alert(typeof box);//类型返回的字符串是undefined 表示未定义


var box = true;//Boolean类型
alert(box);//值是true
alert(typeof box);//类型返回的字符串是boolean


var box = "这是String类型";
alert(box);//值是  这是String类型
alert(typeof box);//类型返回的字符串是string


var box = 888;//Number类型
alert(box);//值是  888
alert(typeof box);//类型返回的字符串是number


//空的对象 表示创建了对象  里面没有内容
//空对象  表示没有创建   就是null
var box = {};//Object类型  与下面的等价
//var box = new Object();//创建空的对象
alert(box);//值是 [object Object]
alert(typeof box);//类型返回的字符串是object


var box = null;//Null类型
alert(box);//值是  null
alert(typeof box);//类型返回的字符串是object


function box(){//box是function函数  
	
	}
alert (box);//值是 function box(){}
alert (typeof(box));////类型返回的字符串是function


//可以直接使用字面量
alert(typeof 123);//number
alert(typeof "测试类型");//string
alert(typeof 'ceshi');//string
alert(typeof null);//object
alert(typeof object);//undefined
alert(typeof Object);//function
alert(typeof function abc(){});//function
alert(typeof new Object())//object

*/