const express = require('express');

var fibonacci = function (number) {
    if (typeof number !== 'number')
        throw new Error('number should be a Number');
    if (number < 0)
        throw new Error('number should >=0')
    if (number > 20)
        throw new Error('number should <= 20');
    if (0 <= number && number <= 1)
        return number;
    return fibonacci(number - 1) + fibonacci(number - 2);
}

var app = express();

app.get('/calc_fibonacci', function (req, res) {
    try {
        var query_n = req.query.n;
        var n = Number(query_n);
        // console.log(query_n, typeof query_n, typeof n);
        res.send(String(fibonacci(n)));
        // res.send(200);
    } catch (e) {
        res.status(500).send(e.message);
    }
});

module.exports = app;

app.listen(3000, function () {
    console.log('server listening at http://127.0.0.1:3000');
});

