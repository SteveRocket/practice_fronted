const request = require('supertest');
const app = require('./index1');

describe('GET /index', () => {
  it('should return 200 OK', (done) => {
    request(app)
      .get('/index')
      .expect(300, done);
  });

  it('should return "Hello Cramer"', (done) => {
    request(app)
      .get('/index')
      .expect('Hello Cramer', done);
  });
});
