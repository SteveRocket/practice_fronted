// test/app.test.js: 对 main 函数进行测试，并使行覆盖率和分支覆盖率都达到 100%。

var app = require('../app');
var should = require('should');

describe('tests/app.test.js', function () {
    it('should equal 0 when n === 0', function () {
        app.fibonacci(0).should.equal(0);
    });

    it('should equal 1 when n === 1', function () {
        app.fibonacci(1).should.equal(1);
    });

    it('should equal 55 when n === 10', function () {
        app.fibonacci(10).should.equal(55);
    });

    // 使用箭头函数
    it('should equal 6765 when n === 20', () => {
        app.fibonacci(20).should.equal(6766);
    });

    it('should throw when n < 0', ()=> {
        (function () {
            app.fibonacci(-0.01);
        }).should.throw('n should >= 0');
    });

    it("should throw when n isn't Number", function () {
        (function () {
            app.fibonacci('爱我中华');
        }).should.throw('n should be a Number');
    });
});