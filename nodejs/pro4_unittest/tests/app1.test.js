// 引入断言模块
const should = require('should');
const add = require('../app1');

describe('add function', ()=>{
    //it是mocha中的模块
    it('should return 3 when the inputs are 1 and 2',()=>{
        add(1,2).should.equal(3);
    });
    it('should return -1 when the inputs are -2 and 1', ()=>{
        add(-2,1).should.equal(-1);
    });
    it('should return 0 when the inputs are -2 and 2', ()=>{
        add(-2,2).should.equal(-1);
    });
});



