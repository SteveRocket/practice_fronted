const assert = require('node:assert');

describe('Math', function() {
  describe('#add()', function() {
    it('should return the sum of two numbers', function() {
      assert.equal(2 + 2, 4);
    });

    it('should return NaN if any of the arguments is not a number', function() {
      assert.ok(isNaN(2 + 'a'));
    });
  });
});