//Node.js 自身的断言
const assert = require('node:assert');
const {add, mul} = require('./main');

console.assert(true);
// assert(false);  // AssertionError [ERR_ASSERTION]: The expression evaluated to a falsy value:
// assert(false, 'This should fail');  // AssertionError [ERR_ASSERTION]: This should fail



assert.strictEqual(1, 1);  // 通过
// assert.strictEqual(1, '1');  // 失败，抛出错误   AssertionError [ERR_ASSERTION]: Expected values to be strictly equal
assert.strictEqual(1 + 2, 3);  // 通过，不会抛出错误
// assert.strictEqual(1 + 2, 4);  // 失败，抛出 AssertionError 错误  AssertionError [ERR_ASSERTION]: Expected values to be strictly equal




assert.equal(add(11, 22, 33), 66);
// assert.equal(add(11, 22, 33), 55);  // AssertionError [ERR_ASSERTION]: 66 == 55
assert.strictEqual(mul(11, 11), 121);
// assert.strictEqual(mul(11, 11), 122);   // AssertionError [ERR_ASSERTION]: Expected values to be strictly equal
assert.strictEqual(22, 33, "22和33不相等");   // AssertionError [ERR_ASSERTION]: 22和33不相等







