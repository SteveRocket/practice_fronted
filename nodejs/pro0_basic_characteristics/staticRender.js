var http = require("http");
var fs = require("fs");    //fs用于读文件

//优点：顶层路由设计

var server = http.createServer(function(req,res){
	console.log("请求：" + req.url);
	//根据用户输入的URL地址根本无法判断本地静态文件的所在路径
	if(req.url == "/test2"){
		fs.readFile("./test2.html",function(err,data){  //readFile("./test2.html"  就是由nodeJS底层的路由设计而实现页面的跳转
			    res.writeHead(200,{"Content-type":"text/html;charset=UTF-8"});
			    res.end(data);
			});
	}else if(req.url == "/test3"){
		fs.readFile("./test3.html",function(err,data){
			    res.writeHead(200,{"Content-type":"text/html;charset=UTF-8"});
			    res.end(data);
			});
	}else if(req.url == "/"){
		//读取当前目录下的HTML文件
		fs.readFile("./test.html",function(err,data){
		    res.writeHead(200,{"Content-type":"text/html;charset=UTF-8"});
		    res.end(data);
		});
	}else if(req.url == "/7.jpg"){  //必须要有这个路由的判断，否则页面上的图片就无法正常显示
		fs.readFile("./7.jpg",function(err,data){
			res.writeHead(200,{"Content-type":"image/jpg;charset=UTF-8"});
		    res.end(data);
		});
	}else if(req.url == "/bg.css"){  //css样式也需要一个路由进行请求加载
		fs.readFile("./bg_color.css",function(err,data){
			res.writeHead(200,{"Content-type":"text/css;charset=UTF-8"});
		    res.end(data);
		});
	}else{
		res.writeHead(404,{"Content-type":"text/html;charset=UTF-8"});
		res.end("当前页面不存在！");
	}
});


server.listen(8090,"0.0.0.0");
console.log("Server running at http://0.0.0.0:8090/");