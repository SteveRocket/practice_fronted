var http = require("http");


var body = "Hello NodeJS";

var server = http.createServer(function(req,res){  //createServer一个工厂方法  返回一个httpServer实例
	console.log("服务器接收的请求：" + req.url + "\tbody.length=" + body.length);
	
	// response.writeHead()//   发送一个返回响应头
	// res.writeHead(200,{"Context-Length":body.length,"Content-Type":"text/plain;charsite=UTF-8"}); //ext/plain表示纯文本
	res.writeHead(200,{"Context-Length":body.length,"Content-Type":"text/html;charsite=UTF-8"});
	
	// res.setHeader("Content-Type","text/html");
	// res.setHeader("Set-Cookie",["type=ninja","language=javascript"]);


	res.write("<h2>主标题h2标签</h2>");
	res.write("<h3>主标题h3标签</h3>");
	res.write("<h4>主标题h4标签</h4>");


	//有无res.writeHead  下面res.end在页面显示的内容不一样
	// res.end("<h1>主标题h1标签</h1>");  //OK
	res.end((1+2+3+4).toString());  //此处必须有.toString()方法
	
	// res.write("<h2>主标题h1标签</h2>");  //此处的write会出错  不能放在end后面

	res.end();//这条语句必须有  表示一个信号，该服务器所有返回的头以及内容都已经发送成功
	//如果没有此条end()  则浏览器永远存在挂起状态，最终导致超时
});



server.listen(8090);