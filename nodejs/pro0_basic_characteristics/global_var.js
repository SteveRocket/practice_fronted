// Node.JS全局变量、全局函数、全局对象


// 1. __dirname和__filename
// console.log(__dirname); // D:\source_code\practice_fronted\nodejs\practice_code
// console.log(__filename);  // D:\source_code\practice_fronted\nodejs\practice_code\global_var.js




// 2. process
// console.log(process);

// 启动参数
// console.log(process.argv);
/*
[
  'C:\\Program Files\\nodejs\\node.exe',
  'D:\\source_code\\practice_fronted\\nodejs\\inner_module\\global_var.js'
]
*/

// console.log(process.argv.length);  // 2
// console.log(process.argv[0]);  // C:\Program Files\nodejs\node.exe
// console.log(process.argv[1]);  // D:\source_code\practice_fronted\nodejs\inner_module\global_var.js

// 内存使用率
// console.log(process.memoryUsage());
/*
{
  rss: 20721664,
  heapTotal: 4481024,
  heapUsed: 2820744,
  external: 941779,
  arrayBuffers: 9898
}
*/

// CPU使用率
// console.log(process.cpuUsage());  // { user: 31000, system: 0 }

//运行目录
// console.log(process.cwd());  // D:\source_code\practice_fronted\nodejs\practice_code
//node版本
// console.log(process.version);  // v14.19.3

// 用户环境
// console.log(process.versions);
/*
{
  node: '14.19.3',
  v8: '8.4.371.23-node.87',
  uv: '1.42.0',
  zlib: '1.2.11',
  brotli: '1.0.9',
  ares: '1.18.1',
  modules: '83',
  nghttp2: '1.42.0',
  napi: '8',
  llhttp: '2.1.4',
  openssl: '1.1.1o',
  cldr: '40.0',
  icu: '70.1',
  tz: '2021a3',
  unicode: '14.0'
}
*/

// cpu架构
// console.log(process.arch);  // x64

// console.log(process.env); // 输出系统环境变量 以及 自己的配置 比如使用vue这类框架开发的时候需要配置测试/开发/生产环境

/*
{
  USERDOMAIN_ROAMINGPROFILE: 'DESKTOP-2N10T7M',
  PROCESSOR_LEVEL: '6',
  NVM_SYMLINK: 'C:\\Program Files\\nodejs',
  MGLS_LICENSE_FILE: 'D:\\steverocket\\modeltech64_10.5\\LICENSE.TXT',
  SESSIONNAME: 'Console',
  ALLUSERSPROFILE: 'C:\\ProgramData',
  PROCESSOR_ARCHITECTURE: 'AMD64',
  ......
}
*/
/*
// 系统平台
console.log(process.platform);  // win32

console.log(process.pid);  // 14428
console.log(process.ppid);  // 19480

// 程序运行的时间
console.log(process.uptime());  // 0.0758337


// 事件监听
process.on('exit', ()=>{
    console.log('程序退出');
});

process.on('beforeExit', ()=>{
    console.log('程序退出之前');
});
// 依次输出：
// 程序退出之前
// 程序退出



// 标准的输入/输出/错误
// 3. console

let name = 'SteveRocket';
let age = 25;
console.log('Name: ' + name);  // Name: SteveRocket
console.log('Age: ' + age);  // Age: 25
console.log('Name:'+name, "Age:"+age);  // Name:SteveRocket Age:25
console.log(`Name:${name}, Age: ${age}`);  // Name:SteveRocket, Age: 25

let x = 10;
let y = 0;

if (y === 0) {
    console.error('Error: Division by zero');
} else {
    console.log('Result: ' + (x / y));
}

console.log = function (data) {
    process.stdout.write('---' + data)
}

console.log("SteveRocket Say");  // 输出：---SteveRocket Say

console.warn("is warnings");
console.info("is informations");

console.time('Loop');
let num = 0;
for (let i = 0; i < 1000000000; i++) {
    // 一些耗时的操作
    num += i;
}
console.log(num);
console.timeEnd('Loop');



// 7. global
global.console.log("Hello SteveRocket");
*/


const util = require('util');
function log() {
    process.stdout.write(util.format.apply(util, arguments) + '\n');
}

log("这是一串中文字符串");  // 这是一串中文字符串
log("SteveRocket"); // SteveRocket