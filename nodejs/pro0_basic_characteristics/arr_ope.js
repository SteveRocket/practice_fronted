const arr1 = ['node.js', 'javascript', 'devops']
const arr2 = [111, 222, 11.22, 'steverocket', 'Cramer', false, null];

/*
// 10. includes(element, startIndex)：判断数组是否包含指定元素，如果包含则返回true，否则返回false。
console.log(arr1.includes("devops"));  // true
console.log(arr1.includes("steverocket"));  // false


// 6. slice(startIndex, endIndex)：返回从起始索引到结束索引之间的元素，不包括结束索引对应的元素。
console.log(arr2.slice(2));   // [ 11.22, 'steverocket', 'Cramer', false, null ]
console.log(arr2.slice(1, 2));  // [ 222 ]

// 修改数组指定位置的元素
arr2[1] = 30000000000
console.log(arr2);  // [ 111, 30000000000, 11.22, 'steverocket', 'Cramer', false, null ]
console.log("数组长度", arr2.length);  // 数组长度 7


// 1. push(element1, element2, ..., elementN)：向数组的末尾添加一个或多个元素，并返回新的长度。
arr2.push('new elements');
arr2.push(['new elements', {name:'steverocket'}, {age:28}]);
console.log(arr2);
//[
//   111,
//   30000000000,
//   11.22,
//   'steverocket',
//   'Cramer',
//   false,
//   null,
//   'new elements',
//   [ 'new elements', { name: 'steverocket' }, { age: 28 } ]
// ]


// 2. pop()：从数组的末尾移除最后一个元素，并返回该元素的值。
console.log(arr2.pop());
console.log(arr2.length);


// 3. shift()：从数组的开头移除第一个元素，并返回该元素的值。
console.log(arr2.shift());
console.log(arr2.length);


// 4. unshift(element1, element2, ..., elementN)：向数组的开头添加一个或多个元素，并返回新的长度。
arr2.unshift('start elements',[1, 2, 3, 4], '头部插入元素')
console.log(arr2.length, arr2);
//10 [
//   'start elements',
//   [ 1, 2, 3, 4 ],
//   '头部插入元素',
//   30000000000,
//   11.22,
//   'steverocket',
//   'Cramer',
//   false,
//   null,
//   'new elements'
// ]
*/




// 5. concat(array1, array2, ..., arrayN)：将一个或多个数组与当前数组合并，返回一个新的数组。


// 7. splice(startIndex, deleteCount, element1, element2, ..., elementN)：从指定索引位置开始删除指定数量的元素，并可选地插入新的元素。


// 8. indexOf(element, startIndex)：返回指定元素在数组中第一次出现的索引，如果未找到则返回-1。


// 9. lastIndexOf(element, startIndex)：返回指定元素在数组中最后一次出现的索引，如果未找到则返回-1。


// 11. find(callback)：返回数组中满足条件的第一个元素，如果未找到则返回undefined。


// 12. findIndex(callback)：返回数组中满足条件的第一个元素的索引，如果未找到则返回-1。


// 13. filter(callback)：返回数组中满足条件的所有元素组成的新数组。


// 14. map(callback)：对数组中的每个元素执行回调函数，并返回执行结果组成的新数组。


// 15. reduce(callback, initialValue)：对数组中的每个元素执行回调函数，将结果累积为单个值。


// 16. forEach(callback)：对数组中的每个元素执行回调函数，没有返回值。



// arr.pop()
// arr.map()
// arr.at()

// arr.push()
// arr.reduce()
// arr.forEach()
// arr.indexOf()
// arr.fill()
// arr.reverse()
// arr.clone()
// arr.findIndex()
// arr.flat()
// arr.copyWithin()
// arr.keys()
// arr.at()