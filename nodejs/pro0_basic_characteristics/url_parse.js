var http = require("http");
var url = require("url");
var querystring = require("querystring");


http.createServer(function(req,res){
	var urls = url.parse(req.url, true);

	//获取查询的参数部分
	var queryObj = urls.query;

	var name = queryObj.name;
	var age = queryObj.age;
	var sex = queryObj.sex;

	console.log(name + age + sex);
	res.end("server get requests:" + name + age + sex);
}).listen(8090);