let num = 9898;
// 在字符串开头填充指定长度的字符串
let sTr = num.toString().padStart(8,'0');
console.log(typeof num, typeof sTr);  // 输出：number string
console.log(num, sTr);  // 输出：9898 00009898

// 在字符串末尾填充指定长度的字符串
let sTr2 = num.toString().padEnd(8, '-');
console.log(sTr2);   // 输出：9898----



console.log('SteveRocket'.padEnd(15));          // "abc       "
console.log('SteveRocket'.padEnd(15, "foo"));   // "abcfoofoof"
console.log('SteveRocket'.padEnd(15, "123456")); // "abc123"
console.log('SteveRocket'.padEnd(1));           // "abc"


// Javascript中对空string调用split返回不是空数组
//理论上应该是没有元素的数组，长度居然是1。查了半天，原来是Javascript中的split和其他语言中不同，即对空string使用split会返回含有一个空string的数组，而不是一个空数组。
var str1 = "";
arr = str1.split("-");
console.log(str1, str1.length);  // 0
//When the string is empty, split returns an array containing one empty string, rather than an empty array.
console.log(arr, arr.length, arr === [], arr===[''], arr===[""]); // [ '' ]  1  false  false  false