var http = require("http");
var url = require("url");
var querystring = require("querystring");

http.createServer(function(req, res){
	//req在API中的IncomingMessage
	console.log(req.url);//
	//http://localhost:8090/show/index.html?id=123   /show/index.html?id=123
	//http://localhost:8090/show/index.html?id=123#    //最后带有#无法识别

	// console.log(req.domain);//null
	// console.log(req.method);//获取请求方式：get、post
	// console.log(req);


	//http://localhost:8090/show/index.html?id=123&name=nodejs
	// var urls = url.parse(req.url);
	// console.log(urls.pathname); // /show/index.html
	// console.log(urls.hostname); //null
	// console.log(urls.port); //null
	// console.log(urls.path);  // /show/index.html?id=123&name=nodejs
	// console.log(urls.query); // id=123&name=nodejs
	// console.log(urls.hash);  //null


	var urls2 = url.parse(req.url,true);
	//parse中添加第二个参数true 获取query的key-value对
	console.log(urls2.query); //{ id: '123', name: 'nodejs' }
	console.log(urls2.query.name);  // nodejs


	// url.format();
	// url.resolve();//从到

	// querystring.stringify();

	//解析查询的参数
	console.log(querystring.parse("name=rocket&num=123&age=24&love"));
	//{ name: 'rocket', num: '123', age: '24', love: '' }

	// console.log(querystring.parse("w=%D6%D0%CE%C4%foo=bar", null, null, {decodeURIComponent:gbkDecodeURIComponent}));

	res.write("hello");
	// querystring.escape;
	// querystring.unescape;



	res.end();
}).listen(8090);