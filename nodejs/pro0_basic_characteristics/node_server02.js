const http = require('node:http');

const hostname = '127.0.0.1';
const port = 3000;


var middlewareLists = [
  function requestBody(req, res, next) {
    parseBody(req, function(err, body) {
      if (err) return next(err);
      req.body = body;
      next();
    });
  },
  function searchDB(req, res, next) {
    checkIdInDatabase(req.body.id, function(err, rows) {
      if (err) return next(err);
      res.dbResult = rows;
      next();
    });
  },
  function isResult(req, res, next) {
    if (res.dbResult && res.dbResult.length > 0) {
      res.end('true');
    }
    else {
      res.end('false');
    }
    next();
  }
]

// createServer中的箭头函数 就是所有http请求的响应函数，即所有的请求都经过这个函数的处理，是所有请求的入口函数。
// 通过createServer函数的第一个参数 可以实现一些我们的业务逻辑，比如需要实现一个接口，获取请求体中的user id，然后根据user id查询库里面的用户信息，来做些判断，比如判断用户角色（是否是管理员）等
const server = http.createServer((req, res) => {
  // res.statusCode = 200;
  // res.setHeader('Content-Type', 'text/plain');
  // res.end('Hello Cramer');

  var i = 0;
  //由middlewares链式调用
  function next(err) {
    if (err){
      return res.end('error:', err.toString());
    }

    // 此处通过middlewareLists 和 next完成了业务逻辑的 `链式调用`，而middlewareLists里的每个函数，都是一个 `中间件`。
    if (i<middlewareLists.length){
      middlewareLists[i++](req,res,next);
    } else {
      return ;
    }
  }
  //触发第一个middleware
  next();
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});