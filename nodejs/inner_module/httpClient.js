// 用于创建HTTP客户端
const http = require("http");
var options = {
    hostname: 'www.example.com',
    port: 80,
    path: '/upload',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
};

var optionsLocal = {
    hostname: '127.0.0.1',
    port: 3000,
    path: '/',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: {"name":"SteveRocket", "information":"this is json data"}
};

var request = http.request(optionsLocal, (response)=>{
    console.log(request.method);
});

request.write('Hello SteveRocket');

request.end();



var request2 = http.get('http://localhost:3000/', (response)=>{
    console.log(request2.method);
});

var request3 = http.get('http://localhost:3000/', function (response) {
    var body = [];

    console.log(response.statusCode);
    console.log(response.headers);

    response.on('data', function (chunk) {
        body.push(chunk);
    });

    response.on('end', function () {
        body = Buffer.concat(body);
        console.log(body.toString());
    });
});
