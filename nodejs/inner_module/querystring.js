// 用于处理URL查询字符串
var querystring = require('querystring');

const queryStringParse = querystring.parse('foo=bar&baz=qux&baz=quux&corge');
console.log(queryStringParse);

const queryStringify = querystring.stringify({ foo: 'bar', baz: ['qux', 'quux'], corge: '' })
console.log(queryStringify);


// querystring.encode()
// querystring.decode()

// querystring.escape()
// querystring.unescape()