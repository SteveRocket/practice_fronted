const fs = require('node:fs');

var contents = '这是一段中文的内容SteveRocket';

const buffer = Buffer.from(contents, 'utf8');
console.log(buffer);  // <Buffer e8 bf 99 e6 98 af e4 b8 80 e6 ae b5 e4 b8 ad e6 96 87 e7 9a 84 e5 86 85 e5 ae b9 53 74 65 76 65 52 6f 63 6b 65 74>
console.log(buffer.toString('utf8')) // 这是一段中文的内容SteveRocket


const buffer2 = Buffer.from('hello', 'ascii');
console.log(buffer2.toString('ascii'));  // 输出：hello



var gbkFiles = fs.readFileSync("steverocket.txt", 'binary');
fs.writeFileSync("steverocket02.txt",gbkFiles, 'binary')

console.log(gbkFiles);
console.log(gbkFiles.toString('ascii'));
console.log(gbkFiles.toString('utf-8'));




function readFile(pathName) {

    var bin = fs.readFileSync(pathName);
    console.log(bin, bin[0], bin[1], bin[2]);

    var bin2 = new Buffer.from(bin, 'utf8');
    console.log(bin2, bin2[0], bin2[1], bin2[2]);

    if (bin[0] === 0xEF && bin[1] === 0xBB && bin[2] === 0xBF) {
        console.log('file code:UTF8');
        bin = bin.slice(3);
    }
    bin = bin.slice(3);
    return bin.toString('utf-8');
}


console.log(readFile("steverocket.txt"))


function replaceFile(pathname) {
    var str = fs.readFileSync(pathname, 'binary');
    str = str.replace('foo', 'bar');
    fs.writeFileSync(pathname, str, 'binary');
}
