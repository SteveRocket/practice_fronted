
// 用于创建HTTP服务器和客户端
var http = require('http');

// http.createServer((request, response)=>{
//     var body = [];
//     console.log(request.method);
//     console.log(request.headers);
//
//     response.end("Hello Cramer");
//
//     request.on('data', (chunk)=>{
//         body.push(chunk);
//     });
//
//     request.on('end', ()=>{
//         body = Buffer.concat(body);
//         console.log(body.toString());
//     });
//
// }).listen(3000, '127.0.0.1', ()=>{
//     console.log("server listening 127.0.0.1:3000")
// });



http.createServer((request, response)=>{
    response.writeHead(200, {"Content-Type":"text/plain"})

    request.on('data', (chunk)=>{
        response.write(chunk);
    });

    request.on('end', ()=>{
        response.end();
    });
}).listen(3000, '127.0.0.1', ()=>{
    console.log("server listening 127.0.0.1:3000")
});