var http = require("http");

var server = http.createServer(function(req,res){
	res.end("Socket.IO");
});


var io = require("socket.io")(server);
io.on("connection",function (socket) {
	console.log("one client connecting.....");
});


server.listen(8888,"0.0.0.0");