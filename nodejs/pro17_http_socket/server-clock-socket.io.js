var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
	res.sendfile("public/server-clock.html");
});


io.on('connection', function(socket){
	console.log("a user connected");
});

function tick(){
	var now = new Date().toUTCString();
	io.emit("time", now);
}

setInterval(tick, 1000);

http.listen(3000, function(){
	console.log('listening on *:3000');
});
