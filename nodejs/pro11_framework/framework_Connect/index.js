
function errorHandler(err, req, res, next) {
  // 错误处理逻辑
}

function businessLogicHandler(req, res, next) {
  // 业务逻辑处理逻辑
}

if (errorHandler.length === 4) {
  console.log("errorHandler is an error handling function");
} else {
  console.log("errorHandler is not an error handling function");
}

if (businessLogicHandler.length === 3) {
  console.log("businessLogicHandler is a business logic handling function");
} else {
  console.log("businessLogicHandler is not a business logic handling function");
}
