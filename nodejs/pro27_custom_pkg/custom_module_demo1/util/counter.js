// 定义了一个私有变量num
var num = 0;

function count() {
    return ++num;
}

//在exports对象导出了一个公有方法count
exports.count = count;