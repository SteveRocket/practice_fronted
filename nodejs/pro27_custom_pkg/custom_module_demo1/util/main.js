var scheduler = require('./scheduler');
var information = require('./information');

console.log('main.js模块被加载');

exports.create = function (name) {
    return {
        name: name,
        information: information(),
        scheduler: scheduler()
    };
};