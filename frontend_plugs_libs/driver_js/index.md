
## 概述
Driver.js是一个功能强大且高度可定制的基于原生JavaScript开发的新用户引导库。它没有依赖项，支持所有主要浏览器。

## 安装

npm i driver.js