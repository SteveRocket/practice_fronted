// by zhangxinxu welcome to visit my personal website http://www.zhangxinxu.com/
// zxx.drag v1.0 2010-03-23 元素的拖拽实现

var params = {
	left: 0,
	top: 0,
	currentX: 0,
	currentY: 0,
	flag: false
};
//获取相关CSS属性
var getCss = function(o,key){
	return o.currentStyle? o.currentStyle[key] : document.defaultView.getComputedStyle(o,false)[key]; 	
};

//拖拽的实现
var startDrag = function(bar, target, callback){
	if(getCss(target, "left") !== "auto"){
		params.left = getCss(target, "left");
	}
	if(getCss(target, "top") !== "auto"){
		params.top = getCss(target, "top");
	}
	//o是移动对象
	bar.onmousedown = function(event){
		params.flag = true;
		if(!event){
			event = window.event;
			//防止IE文字选中
			bar.onselectstart = function(){
				return false;
			}  
		}
		var e = event;
		params.currentX = e.clientX;
		params.currentY = e.clientY;
	};
	document.onmouseup = function(){
		params.flag = false;	
		if(getCss(target, "left") !== "auto"){
			params.left = getCss(target, "left");
		}
		if(getCss(target, "top") !== "auto"){
			params.top = getCss(target, "top");
		}
	};
	document.onmousemove = function(event){
		var e = event ? event: window.event;
		if(params.flag){
			var nowX = e.clientX, nowY = e.clientY;
			var disX = nowX - params.currentX, disY = nowY - params.currentY;
			target.style.left = parseInt(params.left) + disX + "px";
			target.style.top = parseInt(params.top) + disY + "px";
		}
		
		if (typeof callback == "function") {
			callback(parseInt(params.left) + disX, parseInt(params.top) + disY);
		}
	}	
};
//

/* 
* jQuery.Resize 
* Date: 2014-12-04 
* http://keleyi.com/ 
*/
$(function () {
//绑定需要拖拽改变大小的元素对象 
bindResize(document.getElementById('darg-size'));
});

function bindResize(el) {
//初始化参数 
var els = el.style,
//鼠标的 X 和 Y 轴坐标 
x = y = 0;
//邪恶的食指 
$(el).mousedown(function (e) {
//按下元素后，计算当前鼠标与对象计算后的坐标 
x = e.clientX - el.offsetWidth,
y = e.clientY - el.offsetHeight;
//在支持 setCapture 做些东东 
el.setCapture ? (
//捕捉焦点 
el.setCapture(),
//设置事件 
el.onmousemove = function (ev) {
mouseMove(ev || event)
},
el.onmouseup = mouseUp
) : (
//绑定事件 
$(document).bind("mousemove", mouseMove).bind("mouseup", mouseUp)
)
//防止默认事件发生 
e.preventDefault()
});
//移动事件 
function mouseMove(e) {
//宇宙超级无敌运算中... 
els.width = e.clientX - x + 'px',
els.height = e.clientY - y + 'px'
$("#chufa").width(els.width); //给旁边的一个div也同步控制宽
}
//停止事件 
function mouseUp() {
//在支持 releaseCapture 做些东东 
el.releaseCapture ? (
//释放焦点 
el.releaseCapture(),
//移除事件 
el.onmousemove = el.onmouseup = null
) : (
//卸载事件 
$(document).unbind("mousemove", mouseMove).unbind("mouseup", mouseUp)
)
}
};